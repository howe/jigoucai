package com.jigoucai.mvc;


import com.jigoucai.bean.dict.Dict;
import com.jigoucai.bean.param.req.BaseReq;
import com.jigoucai.entity.user.Token;
import com.jigoucai.service.JedisService;
import com.jigoucai.util.Code;
import com.jigoucai.util.Util;
import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.ActionContext;
import org.nutz.mvc.Mvcs;
import org.nutz.mvc.impl.processor.MethodInvokeProcessor;

/**
 * Created by Jianghao on 2017/9/14
 *
 * @howechiang
 */
public class AuthProcessor extends MethodInvokeProcessor {

    protected static final Log log = Logs.get();

    @Override
    public void process(ActionContext ac) throws Throwable {

        System.out.println("请求接口: " + ac.getRequest().getRequestURI());
        System.out.println("请求参数: " + Json.toJson(ac.getMethodArgs()[0]));

        try {

            if (Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_PAYMENT_WEIXIN_NOTIFY)
                    || Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_CMS_ABOUT)
                    || Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_CMS_QALIST)
                    || Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_IMAGES_UPLOAD)) {
                doNext(ac);
                return;
            }

            if (Strings.equalsIgnoreCase(Dict.METHOD_GET, ac.getRequest().getMethod())) {
                Mvcs.write(ac.getResponse(), Code.resp("SYS_JOB_EXPIRED"), JsonFormat.full().setNullAsEmtry(true));
                return;
            }

            BaseReq req = Lang.map2Object(Lang.obj2nutmap(ac.getMethodArgs()[0]), BaseReq.class);
            if (Strings.isBlank(req.getVersion())) {
                Mvcs.write(ac.getResponse(), Code.resp("SYS_PARAMTER_INVALID", Dict.PARAM_VERSION_NAME), JsonFormat.compact());
                return;
            }

            if (Lang.isEmpty(req.getTimestamp())) {
                Mvcs.write(ac.getResponse(), Code.resp("SYS_PARAMTER_INVALID", Dict.PARAM_TIMESTAMP_NAME), JsonFormat.compact());
                return;
            }

            if (Util.checkTimeout(Times.ts2D(req.getTimestamp()), Dict.DOCKING_TIME)) {
                Mvcs.write(ac.getResponse(), Code.resp("SYS_TIMESTAMP_TIMEOUT"), JsonFormat.compact());
                return;
            }

            if (Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_USER_LOGIN_PATH)
                    || Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_USER_SLOGIN_PATH)
                    || Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_USER_HALL_MENU)
                    || Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_USER_HALL_INDEX)
                    || Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_USER_MLOGIN_PATH)
                    || Strings.equalsIgnoreCase(ac.getRequest().getRequestURI(), Dict.ACTION_USER_SENDCODE_PATH)) {
                doNext(ac);
                return;
            }

            String accessToken = req.getToken();
            String tmp = JedisService.query("user:token:*:" + accessToken);
            if (Strings.isEmpty(tmp)) {
                Mvcs.write(ac.getResponse(), Code.resp("USER_TOKEN_EXPIRED"), JsonFormat.compact());
                return;
            }
            Token token = Json.fromJson(Token.class, tmp);
            if (Lang.isEmpty(token)) {
                Mvcs.write(ac.getResponse(), Code.resp("USER_TOKEN_EXPIRED"), JsonFormat.compact());
                return;
            }

            Integer userId = req.getUserId();
            if (!Lang.isEmpty(userId)) {
                if (!Lang.equals(token.getUserId(), userId)) {
                    Mvcs.write(ac.getResponse(), Code.resp("USER_ID_ERROR"), JsonFormat.compact());
                    return;
                }
            }
            doNext(ac);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            Mvcs.write(ac.getResponse(), Code.resp("SYS_ERROR"), JsonFormat.compact());
            return;
        }
    }

}
