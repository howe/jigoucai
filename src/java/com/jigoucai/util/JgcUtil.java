package com.jigoucai.util;

import com.jigoucai.bean.param.resp.BaseResp;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.lang.random.R;
import org.nutz.lang.util.NutMap;

import java.util.Map;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class JgcUtil {

    public static BaseResp buildSuccessResp(Object obj) {
        BaseResp resp = new BaseResp();
        resp.setStatus("SUCCESS");
        resp.setMessage("");
        resp.setData(Lang.obj2nutmap(obj));
        return resp;
    }

    public static BaseResp buildSuccessResp(NutMap map) {
        BaseResp resp = new BaseResp();
        resp.setStatus("SUCCESS");
        resp.setMessage("");
        resp.setData(map);
        return resp;
    }

    public static BaseResp buildSuccessResp(String message, NutMap map) {
        BaseResp resp = new BaseResp();
        resp.setStatus("SUCCESS");
        resp.setMessage(message);
        resp.setData(map);
        return resp;
    }

    public static BaseResp buildSuccessResp(String message) {
        BaseResp resp = new BaseResp();
        resp.setStatus("SUCCESS");
        resp.setMessage(message);
        return resp;
    }

    public static BaseResp buildErrorResp(String code, String message) {
        BaseResp resp = new BaseResp();
        resp.setStatus(code);
        resp.setMessage(message);
        return resp;
    }

    public static Map<String, Object> setData(String name, Object val) {
        NutMap data = new NutMap();
        data.put(name, val);
        return data;
    }

    public static String generateUsername(String prefix) {
        return (Strings.isBlank(prefix) ? "JGC" : prefix.toUpperCase()) + Strings.alignLeft(R.random(0, 999999999), 9, '0') + R.random(0, 9);
    }

    public static String generateTradeNo(String prefix) {
        return (Strings.isBlank(prefix) ? "JGC" : prefix.toUpperCase()) + Times.format("yyyyMMddHHmmssSSS", Times.now()) + Strings.alignLeft(R.random(0, 99999999), 8, '0');
    }

    public static String getLoginSource(String username) {
        if (Strings.isMobile(username)) {
            return "PHONE";
        } else if (Strings.isEmail(username)) {
            return "EMAIL";
        } else {
            return "USERNAME";
        }
    }

    public static String checkCodeAction(String action) {
        switch (action) {
            case "REG":
                return "";
            case "RESET_PWD":
                return "";
            case "BIND":
                return "";
            case "UNBIND":
                return "";
            case "LOGIN":
                return "";
            case "PAYMENT":
                return "";
            default:
                return "不支持的方式";
        }
    }
}
