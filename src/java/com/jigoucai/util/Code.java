package com.jigoucai.util;

import com.jigoucai.bean.param.resp.BaseResp;
import com.jigoucai.util.code.*;
import org.nutz.lang.Strings;

/**
 * Created by Jianghao on 2017/9/13
 *
 * @howechiang
 */
public class Code {

    public static BaseResp resp(String status, String s, String x) {

        status = status.toUpperCase();
        if (Strings.equalsIgnoreCase(status, "SUCCESS")) {
            return new BaseResp("SUCCESS", "");
        } else if (status.startsWith("SYS_")) {
            if (Strings.equalsIgnoreCase(status, "SYS_INVALID_VALUE")
                    || Strings.equalsIgnoreCase(status, "SYS_DATA_NOT_FOUND")
                    || Strings.equalsIgnoreCase(status, "SYS_PARAMTER_INVALID")
                    || Strings.equalsIgnoreCase(status, "SYS_CANT_MODIFY_ORDER")
                    || Strings.equalsIgnoreCase(status, "SYS_EXCEED_MAXIMUM")
                    || Strings.equalsIgnoreCase(status, "SYS_PARAMTER_FORMAT_ERROR")) {
                return JgcUtil.buildErrorResp(status, SYS.map().get(status).replaceAll("%s", s));
            } else if (Strings.equalsIgnoreCase(status, "SYS_BUSY") || Strings.equalsIgnoreCase(status, "SYS_NOT_SUPPORT")) {
                return JgcUtil.buildErrorResp(status, SYS.map().get(status).replaceAll("%s", s).replaceAll("%x", x));
            } else {
                return JgcUtil.buildErrorResp(status, SYS.map().get(status));
            }
        } else if (status.startsWith("USER_")) {
            if (Strings.equalsIgnoreCase(status, "USER_PARAMTER_MISSING")
                    || Strings.equalsIgnoreCase(status, "USER_PARAMTER_FORMAT_ERROR")
                    || Strings.equalsIgnoreCase(status, "USER_AUTHCODE_SEND_ERROR")
                    || Strings.equalsIgnoreCase(status, "USER_OBJ_ALREADY_EXISTS")) {
                return JgcUtil.buildErrorResp(status, USER.map().get(status).replaceAll("%s", s));
            } else {
                return JgcUtil.buildErrorResp(status, USER.map().get(status));
            }
        } else if (status.startsWith("JDG_")) {
            return JgcUtil.buildErrorResp(status, JGC.map().get(status));
        } else {
            return JgcUtil.buildErrorResp("SYS_UNKNOW_ERROR", "未知错误");
        }
    }

    public static BaseResp resp(String status, String s) {
        return resp(status, s, "");
    }

    public static BaseResp resp(String status) {
        return resp(status, "", "");
    }
}
