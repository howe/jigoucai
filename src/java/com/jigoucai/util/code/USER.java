package com.jigoucai.util.code;

import java.util.HashMap;
import java.util.Map;

/**
 * Created  on 2017/9/1
 *
 * @author Jianghao(howechiang@gmail.com)
 */
public class USER {

    public static Map<String, String> map() {
        Map<String, String> map = new HashMap<>();
        map.put("USER_PARAMTER_MISSING", "（%s）不能为空");
        map.put("USER_NOT_FOUND", "用户不存在");
        map.put("USER_USERNAME_PASSWORD_ERROR", "用户名或密码错误");
        map.put("USER_PASSWORD_LOGIN_IS_NOT_SUPPORTED", "不支持密码方式登录");

        map.put("USER_HAS_BEEN_AUTHENTICATED", "用户已实名认证");
        map.put("USER_UNAUTHENTICATED", "用户未实名认证");
        map.put("USER_OBJ_ALREADY_EXISTS", "（%s）已存在");
        map.put("USER_MOBILE_ISUSED", "此手机号码已绑定");
        map.put("USER_MOBILE_BINDED", "用户已绑定手机");
        map.put("USER_AUTHCODE_UNSUPPORTED_USE", "验证码不支持此用途");
        map.put("USER_AUTHCODE_VALIDATION_ERROR", "验证码验证错误");
        map.put("USER_AUTHCODE_SPACING_IS_TOO_SHORT", "发送验证码间隔太短");
        map.put("USER_AUTHCODE_TIME_LIMIT", "验证码已过时效期");
        map.put("USER_AUTHCODE_SEND_ERROR", "%s");
        map.put("USER_GET_INFO_ERROR", "获取信息失败");
        map.put("USER_NO_SESSION", "无会话信息");
        map.put("USER_TOKEN_EXPIRED", "令牌已失效");
        map.put("USER_UNBUNDLED_MOBILE", "用户未绑定手机");
        map.put("USER_ID_ERROR", "令牌对应的用户IU错误");
        map.put("USER_IS_FROZEN", "用户账号已冻结");
        map.put("USER_PASSWORD_IS_ERROR", "密码错误");
        map.put("USER_PAYPASSWORD_IS_ERROR", "支付密码错误");
        map.put("USER_MARGIN_INSUFFICIENT", "保证金不足");
        map.put("USER_AMOUNT_ERROR", "充值金额错误");

        map.put("USER_BALANCE_INSUFFICIENT", "余额不足");
        return map;
    }
}
