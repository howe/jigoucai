package com.jigoucai.util.code;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 2017/9/1
 *
 * @author Jianghao(howechiang@gmail.com)
 */
public class SYS {

    public static Map<String, String> map(){
        Map<String, String> map = new HashMap<>();
        map.put("SUCCESS", "");
        map.put("SYS_ERROR", "系统异常");
        map.put("SYS_SERVICE_UNAVAILABLE", "服务不可用");
        map.put("SYS_PARAMTER_FORMAT_ERROR", "（%s）参数格式错误");
        map.put("SYS_UNDER_MAINTENANCE", "系统正在维护中");
        map.put("SYS_PARAMTER_INVALID", "缺少必要的参数（%s）");
        map.put("SYS_BUSY", "无效的参数值(期望的是%s，实际为%x)");
        map.put("SYS_API_NOT_FOUND", "任务处理超时");
        map.put("SYS_JOB_EXPIRED", "当前接口不支持请求的HTTP METHOD，请检查是否选择了正确的 POST/GET 请求方式。");
        map.put("SYS_ILLEGAL_REQUEST", "非法的请求");
        map.put("SYS_PERMISSION_DENIED", "该资源需要拥有授权才能调用");
        map.put("SYS_APPID_INVALID", "调用者提供的 app_id 参数无效");
        map.put("SYS_IP_LIMIT", "请求方所处的 IP 不允许访问该资源");
        map.put("SYS_TIMESTAMP_TIMEOUT", "请求时间戳超时");
        map.put("SYS_DATA_NOT_FOUND", "此（%s）数据不存在");
        map.put("SYS_INVALID_VALUE", "无效的参数值(%s)");
        map.put("SYS_PARTNER_NOT_FOUND", "此ID的合作商不存在");
        map.put("SYS_PARTNER_IS_LOCK", "合作商已锁定");
        map.put("SYS_PROVIDER_IS_LOCK", "服务商已锁定");
        map.put("SYS_VERSION_NOT_SUPPORT", "暂只支持1.0版本");
        map.put("SYS_SIGN_IS_ERROR", "签名错误");
        map.put("SYS_PAYPASSWORD_IS_ERROR", "支付密码错误");
        map.put("SYS_CANT_MODIFY_ORDER", "无法修改订单（%s）");
        map.put("SYS_EXCEED_MAXIMUM", "超过最大%s");
        map.put("SYS_NOT_SUPPORT", "%s暂只支持%x");
        return map;
    }
}
