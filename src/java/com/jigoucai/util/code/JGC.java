package com.jigoucai.util.code;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 2017/9/13
 *
 * @author Jianghao(howechiang@gmail.com)
 */
public class JGC {

    public static Map<String, String> map() {
        Map<String, String> map = new HashMap<>();
        map.put("JGC_PARTNER_INSUFFICIENT_BALANCE", "采购账号余额不足");
        map.put("JGC_ORDER_CANCEL", "订单已取消");
        map.put("JGC_ORDER_CREATED", "订单已创建，待支付");
        map.put("JGC_ORDER_PAID", "订单已支付，待处理");
        map.put("JGC_ORDER_PROCESSING", "订单处理中");
        map.put("JGC_ORDER_SHIPPED", "订单已发货，待收货确认");
        map.put("JGC_ORDER_COMPLETED", "订单已完成");
        map.put("JGC_PURCHASE_NOT_SUPPORT", "采购类型不支持");
        map.put("JGC_PARTNER_NOT_FOUND", "采购账号不存在");
        map.put("JGC_PARTNER_IS_LOCKED", "采购账号已锁定");
        map.put("JGC_PARTNER_INSUFFICIENT_BALANCE", "采购账号余额不足");
        map.put("JGC_PROVIDER_ABNORMAL", "服务商状态异常");
        map.put("JGC_ORDER_NOT_FOUND", "订单不存在或已被他人受理");
        map.put("JGC_NO_ORDER_DATA", "没有订单数据");
        return map;
    }
}
