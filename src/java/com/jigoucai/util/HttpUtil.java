package com.jigoucai.util;

import org.nutz.http.*;
import org.nutz.json.Json;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;

import java.util.Map;

/**
 * Created by Jianghao on 2017/3/14.
 *
 * @author Howe(howechiang@gmail.com)
 */
public class HttpUtil {

    /**
     * 普通Get请求
     *
     * @param url
     * @return
     */
    public static String get(String url) {

        if (Strings.isBlank(url))
            return null;
        else if (!Strings.isUrl(url))
            return null;
        else
            return Http.get(url).getContent();
    }

    /**
     * 普通Post请求
     *
     * @param url
     * @param parms
     * @return
     */
    public static String post(String url, Map<String, Object> parms) {

        if (Strings.isBlank(url))
            return null;
        else if (!Strings.isUrl(url))
            return null;
        else if (parms.isEmpty())
            return null;
        else
            return Http.post(url, parms, 15000);
    }

    /**
     * 带Header信息的Post请求
     *
     * @param url
     * @param header
     * @param parms
     * @return
     */
    public static String post(String url, Header header, Map<String, Object> parms) {

        if (Strings.isBlank(url))
            return null;
        else if (!Strings.isUrl(url))
            return null;
        else if (Lang.isEmpty(header))
            return null;
        else if (header.getAll().isEmpty())
            return null;
        else
            return Http.post3(url, parms, header, 15000).getContent();
    }

    /**
     * 带Header信息Get请求
     *
     * @param url
     * @param header
     * @return
     */
    public static String get(String url, Header header) {

        if (Strings.isBlank(url))
            return null;
        else if (!Strings.isUrl(url))
            return null;
        else if (Lang.isEmpty(header))
            return null;
        else if (header.getAll().isEmpty())
            return null;
        else
            return Http.get(url, header, 15000).getContent();
    }

    /**
     * Post发送Json请求
     *
     * @param url
     * @param json
     * @return
     */
    public static String post(String url, String json) {

        if (Strings.isBlank(url))
            return null;
        else if (!Strings.isUrl(url))
            return null;
        else if (Lang.isEmpty(json))
            return null;
        else {
            Request req = Request.create(url, Request.METHOD.POST);
            req.getHeader().set("Content-Type", "application/json;charset=UTF-8");
            req.setData(json.toString());
            Response resp = Sender.create(req).setTimeout(15000).send();
            return resp.getContent();
        }

    }

    /**
     * 无参数的Post请求
     *
     * @param url
     * @return
     */
    public static String post(String url) {

        if (Strings.isBlank(url))
            return null;
        else if (!Strings.isUrl(url))
            return null;
        else
            return Http.post(url, null, 15000);
    }

    /**
     * Post发送Xml请求
     *
     * @param url
     * @param xml
     * @return
     */
    public static String postXml(String url, String xml) {

        if (Strings.isBlank(url))
            return null;
        else if (!Strings.isUrl(url))
            return null;
        else if (Lang.isEmpty(xml))
            return null;
        else {
            return Http.postXML(url, xml, 15000).getContent();
        }
    }
}
