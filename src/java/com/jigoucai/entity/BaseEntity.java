package com.jigoucai.entity;

import java.io.Serializable;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang@gmail.com)
 */
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
}
