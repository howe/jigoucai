package com.jigoucai.entity.hall;

import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.json.JsonField;

import java.util.List;

/**
 * 大厅菜单表
 * Created on 2018/3/17
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
@Table("hall_menu")
public class Menu extends BaseEntity {

    /**
     * ID
     */
    @Id
    @Column("id")
    private Integer menuId;

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    /**
     * 菜单名称
     */
    @Column("menu_name")
    private String menuName;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    /**
     * 菜单键值
     */
    @Column("menu_key")
    private String menuKey;

    public String getMenuKey() {
        return menuKey;
    }

    public void setMenuKey(String menuKey) {
        this.menuKey = menuKey;
    }

    /**
     * 菜单参数
     */
    @Column("menu_val")
    private String menuVal;

    public String getMenuVal() {
        return menuVal;
    }

    public void setMenuVal(String menuVal) {
        this.menuVal = menuVal;
    }

    /**
     * 是否显示
     */
    @JsonField(ignore = true)
    @Column("is_display")
    private boolean isDisplay;

    public boolean isDisplay() {
        return isDisplay;
    }

    public void setDisplay(boolean display) {
        isDisplay = display;
    }

    /**
     * 父级ID
     */
    @JsonField(ignore = true)
    @Column("parent_id")
    private int parentId;

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Many(target = Menu.class, key = "menuId", field = "parentId")
    private List<Menu> subList;

    public List<Menu> getSubList() {
        return subList;
    }

    public void setSubList(List<Menu> subList) {
        this.subList = subList;
    }
}
