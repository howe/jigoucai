package com.jigoucai.entity.hall;

import com.jigoucai.bean.dict.Dict;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.json.JsonField;

import java.util.Date;

/**
 * Created on 2018/3/17
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
@Table("hall_banner")
public class Banner extends BaseEntity {

    /**
     * ID
     */
    @Id
    @Column("id")
    private int bannerId;

    public int getBannerId() {
        return bannerId;
    }

    public void setBannerId(int bannerId) {
        this.bannerId = bannerId;
    }

    /**
     * 标题
     */
    @Column("title")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 图片链接
     */
    @Column("image_url")
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * 跳转链接
     */
    @Column("link")
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    /**
     * 添加时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL, timeZone = Dict.DATE_CHINA_TIMEZONE)
    @Column("add_time")
    private Date addTime;

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * 是否显示
     */
    @Column("is_display")
    private boolean isDisplay;

    public boolean isDisplay() {
        return isDisplay;
    }

    public void setDisplay(boolean display) {
        isDisplay = display;
    }
}
