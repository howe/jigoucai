package com.jigoucai.entity.hall;

import com.jigoucai.bean.dict.Dict;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.json.JsonField;

import java.util.Date;

/**
 * Created on 2018/3/30
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
@Table("hall_marquee")
public class Marquee extends BaseEntity {

    /**
     * ID
     */
    @Id
    @Column("id")
    private int marqueeId;

    public int getMarqueeId() {
        return marqueeId;
    }

    public void setMarqueeId(int marqueeId) {
        this.marqueeId = marqueeId;
    }

    /**
     * 内容
     */
    @Column("content")
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 链接
     */
    @Column("link")
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    /**
     * 推送时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL, timeZone = Dict.DATE_CHINA_TIMEZONE)
    @Column("push_time")
    private String pushTime;

    public String getPushTime() {
        return pushTime;
    }

    public void setPushTime(String pushTime) {
        this.pushTime = pushTime;
    }

    /**
     * 失效时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL, timeZone = Dict.DATE_CHINA_TIMEZONE, ignore = true)
    @Column("failure_time")
    private Date failureTime;

    public Date getFailureTime() {
        return failureTime;
    }

    public void setFailureTime(Date failureTime) {
        this.failureTime = failureTime;
    }
}
