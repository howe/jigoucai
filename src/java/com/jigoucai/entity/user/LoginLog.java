package com.jigoucai.entity.user;

import com.jigoucai.bean.dict.Dict;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.json.JsonField;
import org.nutz.lang.util.NutMap;

/**
 *
 */
@Table("user_login_log")
public class LoginLog extends BaseEntity {

    /**
     * ID
     */
    @Id
    @Column("id")
    private Integer loginLogId;

    public Integer getLoginLogId() {
        return loginLogId;
    }

    public void setLoginLogId(Integer loginLogId) {
        this.loginLogId = loginLogId;
    }

    /**
     * 用户ID
     */
    @Column("user_id")
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 登录类型：
     * 账号来源。有以下值之一：
     * WECHAT:通过微信账号登录；
     * WEIBO：通过新浪微博账号登录；
     * QQ：通过 QQ 账号登录；
     * EMAIL：通过电子邮件；
     * PHONE：通过手机号码。
     * 其他的第三方单独定义
     */
    @Column("source")
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    /**
     * 登录时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL)
    @Column("login_time")
    private java.util.Date loginTime;

    public java.util.Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(java.util.Date loginTime) {
        this.loginTime = loginTime;
    }

    /**
     * 登录IP
     */
    @Column("login_ip")
    private String loginIp;

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    /**
     * 登录来源
     * 游戏或APP等
     */
    @Column("app_from")
    private String appFrom;

    public String getAppFrom() {
        return appFrom;
    }

    public void setAppFrom(String appFrom) {
        this.appFrom = appFrom;
    }

    /**
     * agent信息
     */
    @Column("login_agent")
    private String loginAgent;

    public String getLoginAgent() {
        return loginAgent;
    }

    public void setLoginAgent(String loginAgent) {
        this.loginAgent = loginAgent;
    }

    /**
     * 设备信息
     */
    @ColDefine(type = ColType.MYSQL_JSON)
    @Column("device")
    private NutMap device;

    public NutMap getDevice() {
        return device;
    }

    public void setDevice(NutMap device) {
        this.device = device;
    }
}