package com.jigoucai.entity.user;

import com.jigoucai.bean.biz.CashAccount;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.json.JsonField;

import java.util.List;

/**
 *
 */
@Table("user_profile")
public class User extends BaseEntity {

    /**
     * 用户ID
     */
    @Id
    @Column("id")
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 昵称
     */
    @Column("nickname")
    private String nickName;

    public String getNickName() {

        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * 用户名。如果用户使用手机号、邮箱或来自于第三方，则该字段根据规则生成一个随机的用户名。
     * 建议的规则是来源渠道的：首字母缩写+10个左右的不会和其他用户重复的随机数字
     */
    @Column("username")
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 手机号码
     */
    @Column("mobile")
    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 用户密码，如果该字段为 NULL，则表示该用户还没有设置密码。
     * 当用户绑定了 phone 或/和 email、之后，才需要设置密码。
     */
    @Column("password")
    @JsonField(ignore = true)
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 保存密码时使用的随机字符串。
     */
    @Column("salt")
    @JsonField(ignore = true)
    private String salt;

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * 用户真实姓名
     */
    @Column("realname")
    private String realname;

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    /**
     * 身份证号码
     */
    @Column("id_card")
    private String idCard;

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    /**
     * 实名认证状态。
     *
     */
    @Column("is_auth")
    private boolean isAuth;

    public boolean isAuth() {
        return isAuth;
    }

    public void setAuth(boolean auth) {
        isAuth = auth;
    }

    /**
     * 用户最新的登录日志信息
     */
    @Column("last_login_log")
    @ColDefine(type = ColType.MYSQL_JSON)
    private LoginLog lastLoginLog;

    public LoginLog getLastLoginLog() {
        return lastLoginLog;
    }

    public void setLastLoginLog(LoginLog lastLoginLog) {
        this.lastLoginLog = lastLoginLog;
    }

    /**
     * 用户头像
     */
    @Column("avatar_url")
    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    /**
     * 账号状态
     * 0 冻结
     * 1 正常
     */
    @Column("is_lock")
    private boolean isLock;

    public boolean isLock() {
        return isLock;
    }

    public void setLock(boolean lock) {
        isLock = lock;
    }

    /**
     * 支付密码
     */
    @Column("pay_password")
    private String payPassword;

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    /**
     * 提现账号
     */
    @Column("cash_accounts")
    @ColDefine(type = ColType.MYSQL_JSON)
    private List<CashAccount> cashAccounts;

    public List<CashAccount> getCashAccounts() {
        return cashAccounts;
    }

    public void setCashAccounts(List<CashAccount> cashAccounts) {
        this.cashAccounts = cashAccounts;
    }
}