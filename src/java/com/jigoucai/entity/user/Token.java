package com.jigoucai.entity.user;

import com.jigoucai.bean.dict.Dict;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.json.JsonField;

import java.util.Date;

/**
 * Created by Jianghao on 2017/9/1
 *
 * @howechiang
 */
@Table("user_token")
public class Token extends BaseEntity {

    /**
     * 用户ID
     */
    @Id(auto = false)
    @Column("user_id")
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 访问令牌
     */
    @Column("access_token")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * 生成时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL)
    @Column("create_time")
    private Date createTime;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 失效时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL)
    @Column("failure_Time")
    private Date failureTime;

    public Date getFailureTime() {
        return failureTime;
    }

    public void setFailureTime(Date failureTime) {
        this.failureTime = failureTime;
    }
}
