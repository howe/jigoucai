package com.jigoucai.entity.user;

import com.jigoucai.bean.dict.Dict;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.json.JsonField;
import org.nutz.lang.util.NutMap;

/**
 * 注册
 */
@Table("user_reg_log")
public class RegLog extends BaseEntity {

    /**
     * ID
     */
    @Id(auto = false)
    @Column("user_id")
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 注册时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL)
    @Column("reg_time")
    private java.util.Date regTime;

    public java.util.Date getRegTime() {
        return regTime;
    }

    public void setRegTime(java.util.Date regTime) {
        this.regTime = regTime;
    }

    /**
     * 通过什么方式注册的。
     * 通过什么方式注册的。有以下值之一：
     * WECHAT:通过微信账号登录注册；
     * WEIBO：通过新浪微博账号登录注册；
     * QQ：通过 QQ 账号登录注册；
     * EMAIL：通过电子邮件注册；
     * PHONE：通过手机号码注注；
     * USERNAME：通过手机号码注注；
     * 或者其他的第三方标识。
     */
    @Column("source")
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    /**
     * 在我们的哪个 app 或站点注册的。
     * app编号|app名称
     */
    @Column("app_from")
    private String appFrom;

    public String getAppFrom() {
        return appFrom;
    }

    public void setAppFrom(String appFrom) {
        this.appFrom = appFrom;
    }

    /**
     * 设备信息。包含设备型号等信息
     */
    @Column("device")
    @ColDefine(type = ColType.MYSQL_JSON)
    private NutMap device;

    public NutMap getDevice() {
        return device;
    }

    public void setDevice(NutMap device) {
        this.device = device;
    }

    /**
     * agent信息
     */
    @Column("reg_agent")
    private String regAgent;

    public String getRegAgent() {
        return regAgent;
    }

    public void setRegAgent(String regAgent) {
        this.regAgent = regAgent;
    }

    /**
     * 注册IP
     */
    @Column("reg_ip")
    private String regIp;

    public String getRegIp() {
        return regIp;
    }

    public void setRegIp(String regIp) {
        this.regIp = regIp;
    }

    /**
     * 渠道信息。渠道编号|渠道名称
     */
    @Column("channel")
    private String channel;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * 邀请码。
     */
    @Column("promo_code")
    private String promoCode;

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
}