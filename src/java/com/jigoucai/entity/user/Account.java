package com.jigoucai.entity.user;

import com.jigoucai.bean.dict.Dict;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.json.JsonField;
import org.nutz.lang.util.NutMap;

/**
 *
 */
@Table("user_account")
public class Account extends BaseEntity {

    /**
     * ID
     */
    @Id
    @Column("id")
    private Integer accountId;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    /**
     * 该账号所属用户的编号。
     * 同一个用户对于同一个source + open_id的记录只能有一条。
     */
    @Column("user_id")
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 账号在来源方的统一编号。
     */
    @Column("open_id")
    private String openId;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    /**
     * 账号来源。有以下值之一：
     * WEIXIN:通过微信账号登录注册；
     * MINIAPP:通过微信小程序登录注册；
     * WEIBO：通过新浪微博账号登录注册；
     * QQ：通过 QQ 账号登录注册；
     * 其他的第三方单独定义。
     */
    @Column("source")
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    /**
     * 授权码。如果验证协议是 phone，这里保存的是短信验证码
     */
    @Column("access_token")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * 生成时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL)
    @Column("token_create_time")
    private java.util.Date tokenCreateTime;

    public java.util.Date getTokenCreateTime() {
        return tokenCreateTime;
    }

    public void setTokenCreateTime(java.util.Date tokenCreateTime) {
        this.tokenCreateTime = tokenCreateTime;
    }

    /**
     * 失效时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL)
    @Column("token_expire_time")
    private java.util.Date tokenExpireTime;

    public java.util.Date getTokenExpireTime() {
        return tokenExpireTime;
    }

    public void setTokenExpireTime(java.util.Date tokenExpireTime) {
        this.tokenExpireTime = tokenExpireTime;
    }

    /**
     * 该账号绑定到 user_profile 的时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL)
    @Column("bind_time")
    private java.util.Date bindTime;

    public java.util.Date getBindTime() {
        return bindTime;
    }

    public void setBindTime(java.util.Date bindTime) {
        this.bindTime = bindTime;
    }

    /**
     * 该账号最近一次登录的情况
     */
    @Column("last_login_log")
    @ColDefine(type = ColType.MYSQL_JSON)
    private LoginLog lastLoginLog;

    public LoginLog getLastLoginLog() {
        return lastLoginLog;
    }

    public void setLastLoginLog(LoginLog lastLoginLog) {
        this.lastLoginLog = lastLoginLog;
    }

    /**
     * 预留的可扩展字段。比如从微信登录的话，可以保存微信登录接口返回过来的微信用户数据
     */
    @Column("expand")
    @ColDefine(type = ColType.MYSQL_JSON)
    private NutMap expand;

    public NutMap getExpand() {
        return expand;
    }

    public void setExpand(NutMap expand) {
        this.expand = expand;
    }

    /**
     * 备用字段
     */
    @Column("reserve")
    @ColDefine(type = ColType.MYSQL_JSON)
    private NutMap reserve;

    public NutMap getReserve() {
        return reserve;
    }

    public void setReserve(NutMap reserve) {
        this.reserve = reserve;
    }
}