package com.jigoucai.entity.finance;

import com.jigoucai.bean.biz.CashAccount;
import com.jigoucai.bean.dict.Dict;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.json.JsonField;
import org.nutz.lang.util.NutMap;

import java.util.Date;

/**
 * 资金往来账
 * Created by Jianghao on 2018/2/2
 *
 * @howechiang
 */
@Table("finance_current")
public class Current extends BaseEntity {

    /**
     * ID
     */
    @Id
    @Column("id")
    private Integer currentId;

    public Integer getCurrentId() {
        return currentId;
    }

    public void setCurrentId(Integer currentId) {
        this.currentId = currentId;
    }

    /**
     * 流水号
     */
    @Column("current_num")
    private String currentNum;

    public String getCurrentNum() {
        return currentNum;
    }

    public void setCurrentNum(String currentNum) {
        this.currentNum = currentNum;
    }

    /**
     * 资金归属用户ID
     */
    @Column("user_id")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * 录入时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL, timeZone = Dict.DATE_CHINA_TIMEZONE)
    @Column("entry_time")
    private Date entryTime;

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    /**
     * 金额
     * 单位，分
     */
    @Column("amount")
    private int amount;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * 事项
     * 0 出账
     * 1 进账
     */
    @Column("cause")
    private Boolean cause;

    public Boolean getCause() {
        return cause;
    }

    public void setCause(Boolean cause) {
        this.cause = cause;
    }

    /**
     * 事由
     */
    @Column("reason")
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * 交易流水号
     */
    @Column("tsn")
    private String tsn;

    public String getTsn() {
        return tsn;
    }

    public void setTsn(String tsn) {
        this.tsn = tsn;
    }

    /**
     * 付款账号
     */
    @Column("payment_account")
    @ColDefine(type = ColType.MYSQL_JSON)
    private CashAccount paymentAccount;

    public CashAccount getPaymentAccount() {
        return paymentAccount;
    }

    public void setPaymentAccount(CashAccount paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    /**
     * 收款账号
     */
    @Column("shroff_account")
    @ColDefine(type = ColType.MYSQL_JSON)
    private CashAccount shroffAccount;

    public CashAccount getShroffAccount() {
        return shroffAccount;
    }

    public void setShroffAccount(CashAccount shroffAccount) {
        this.shroffAccount = shroffAccount;
    }

    /**
     * 是否确认
     */
    @Column("is_arrived")
    private boolean isArrived;

    public boolean isArrived() {
        return isArrived;
    }

    public void setArrived(boolean arrived) {
        isArrived = arrived;
    }

    /**
     * 确认时间
     */
    @Column("arrive_time")
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL, timeZone = Dict.DATE_CHINA_TIMEZONE)
    private Date arriveTime;

    public Date getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(Date arriveTime) {
        this.arriveTime = arriveTime;
    }

    /**
     * 支付信息
     */
    @JsonField(ignore = true)
    @Column("expand")
    @ColDefine(type = ColType.MYSQL_JSON)
    private NutMap expand;

    public NutMap getExpand() {
        return expand;
    }

    public void setExpand(NutMap expand) {
        this.expand = expand;
    }
}
