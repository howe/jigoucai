package com.jigoucai.entity.finance;

import com.jigoucai.bean.dict.Dict;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.json.JsonField;
import org.nutz.lang.util.NutMap;

import java.util.Date;

/**
 * 资金交易日志
 * Created by Jianghao on 2018/2/2
 *
 * @howechiang
 */
@Table("finance_fund")
public class Fund extends BaseEntity {

    /**
     * ID
     */
    @Id
    @Column("id")
    private Integer marginId;

    public Integer getMarginId() {
        return marginId;
    }

    public void setMarginId(Integer marginId) {
        this.marginId = marginId;
    }

    /**
     * 流水号
     */
    @Column("fund_num")
    private String fundNum;

    public String getFundNum() {
        return fundNum;
    }

    public void setFundNum(String fundNum) {
        this.fundNum = fundNum;
    }

    /**
     * 资金归属用户ID
     */
    @Column("user_id")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * 记账时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL, timeZone = Dict.DATE_CHINA_TIMEZONE)
    @Column("bill_time")
    private Date billTime;

    public Date getBillTime() {
        return billTime;
    }

    public void setBillTime(Date billTime) {
        this.billTime = billTime;
    }

    /**
     * 金额
     */
    @Column("amount")
    private int amount;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * 事项
     * 0 提现
     * 1 转入
     */
    @Column("cause")
    private Boolean cause;

    public Boolean getCause() {
        return cause;
    }

    public void setCause(Boolean cause) {
        this.cause = cause;
    }

    /**
     * 事由
     */
    @Column("reason")
    private String reason;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * 关联单ID
     */
    @Column("relate_id")
    private Integer relateId;

    public Integer getRelateId() {
        return relateId;
    }

    public void setRelateId(Integer relateId) {
        this.relateId = relateId;
    }

    /**
     * 关联单
     */
    @JsonField(ignore = true)
    @Column("relate")
    @ColDefine(type = ColType.MYSQL_JSON)
    private NutMap relate;

    public NutMap getRelate() {
        return relate;
    }

    public void setRelate(NutMap relate) {
        this.relate = relate;
    }
}
