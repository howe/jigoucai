package com.jigoucai.job;

import com.jigoucai.entity.finance.Current;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.nutz.integration.quartz.annotation.Scheduled;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Stopwatch;
import org.nutz.lang.Times;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.util.List;

/**
 * Created by Jianghao on 2017/2/27.
 */
@IocBean
@Scheduled(cron = "0 0/10 * * * ?")
public class DeleteExpiredDataJob implements Job {

    protected static final Log log = Logs.get();

    @Inject
    protected Dao dao;

    @Override
    public void execute(JobExecutionContext context) {

        Stopwatch sw = Stopwatch.begin();
        try {
            List<Current> currentList = dao.query(Current.class, Cnd.where("isArrived", "=", false));
            currentList.parallelStream().forEach(current -> {
                if (Times.between(current.getEntryTime(), Times.now(), Times.T_1S) > 1800) {
                    dao.delete(current);
                }
            });
            sw.tag("删除超时未确认的流水信息");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            sw.stop();
            System.out.println(sw.toString());
        }
    }
}