package com.jigoucai.bean.param.req.cms;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/4/16
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class CollectFormIdReq extends BaseReq {

    /**
     * formId 用于发送模板消息
     */
    private String formId;

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }
}
