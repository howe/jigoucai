package com.jigoucai.bean.param.req.payment;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created by Jianghao on 2018/3/24
 *
 * @howechiang
 */
public class RequestPaymentReq extends BaseReq {

    /**
     * 充值金额
     */
    private int amount;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
