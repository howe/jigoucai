package com.jigoucai.bean.param.req.user;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created by Jianghao on 2017/11/2
 *
 * @howechiang
 */
public class SendCodeReq extends BaseReq {

    /**
     * 接收对象
     */
    private String target;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * 该安全码的用途。
     * 有以下值之一：
     * REG：注册；
     * ESET_PWD：重置密码；
     * BIND：绑定；
     * UNBIND：取消绑定；
     * WITHDRAW：提现；
     */
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    /**
     * 发送类型
     * EMAIL
     * PHONE
     * PUSH
     */
    private String targetType;

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }
}
