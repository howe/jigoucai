package com.jigoucai.bean.param.req.user;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/4/9
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class RealnameAuthReq extends BaseReq {

    /**
     * 用户真实姓名
     */
    private String realname;

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    /**
     * 身份证号码
     */
    private String idCard;

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
}
