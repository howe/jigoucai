package com.jigoucai.bean.param.req.user;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/3/25
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class CheckMarginReq extends BaseReq {

    private double margin;

    public double getMargin() {
        return margin;
    }

    public void setMargin(double margin) {
        this.margin = margin;
    }
}
