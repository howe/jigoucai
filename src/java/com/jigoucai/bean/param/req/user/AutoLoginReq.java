package com.jigoucai.bean.param.req.user;

import com.jigoucai.bean.biz.App;
import com.jigoucai.bean.param.req.BaseReq;
import org.nutz.lang.util.NutMap;

/**
 * Created on 2018/3/15
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class AutoLoginReq extends BaseReq {

    /**
     * 设备信息
     */
    private NutMap device;

    public NutMap getDevice() {
        return device;
    }

    public void setDevice(NutMap device) {
        this.device = device;
    }

    /**
     * APP信息
     */
    private App app;

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }
}
