package com.jigoucai.bean.param.req.user;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/4/9
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class ModifyPayPswdReq extends BaseReq {

    /**
     * 短信验证码
     */
    private String authCode;

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    /**
     * 支付密码
     */
    private String payPassword;

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }
}
