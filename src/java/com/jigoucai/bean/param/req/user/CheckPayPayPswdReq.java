package com.jigoucai.bean.param.req.user;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/3/25
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class CheckPayPayPswdReq extends BaseReq {

    private String payPassword;

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }
}
