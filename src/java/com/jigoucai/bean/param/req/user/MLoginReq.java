package com.jigoucai.bean.param.req.user;

import com.jigoucai.bean.biz.App;
import com.jigoucai.bean.param.biz.RawData;
import com.jigoucai.bean.param.req.BaseReq;
import org.nutz.lang.util.NutMap;

/**
 * 微信小程序登录
 * Created by Jianghao on 2018/3/15
 *
 * @howechiang
 */
public class MLoginReq extends BaseReq {

    /**
     * 登录方式
     */
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    /**
     * 设备信息
     */
    private NutMap device;

    public NutMap getDevice() {
        return device;
    }

    public void setDevice(NutMap device) {
        this.device = device;
    }

    /**
     * APP信息
     */
    private App app;

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    /**
     * 用户登录凭证
     */
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 用户信息
     */
    private RawData rawData;

    public RawData getRawData() {
        return rawData;
    }

    public void setRawData(RawData rawData) {
        this.rawData = rawData;
    }
}
