package com.jigoucai.bean.param.req.user;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/3/15
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class BindPhoneReq extends BaseReq {

    /**
     * 手机号码
     */
    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 短信验证码
     */
    private String authCode;

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }
}
