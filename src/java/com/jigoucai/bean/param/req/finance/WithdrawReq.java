package com.jigoucai.bean.param.req.finance;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/4/4
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class WithdrawReq extends BaseReq {

    private double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    private String bankName;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
