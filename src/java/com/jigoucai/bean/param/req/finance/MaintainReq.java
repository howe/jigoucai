package com.jigoucai.bean.param.req.finance;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/4/9
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class MaintainReq extends BaseReq {

    /**
     * 开户行
     */
    private String bankName;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * 账户
     */
    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    private String payPassword;

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }
}
