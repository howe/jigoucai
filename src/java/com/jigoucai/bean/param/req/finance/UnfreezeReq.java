package com.jigoucai.bean.param.req.finance;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/4/9
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class UnfreezeReq extends BaseReq {

    /**
     * 支付密码
     */
    private String payPassword;

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }
}
