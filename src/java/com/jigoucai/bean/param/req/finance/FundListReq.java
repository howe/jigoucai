package com.jigoucai.bean.param.req.finance;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/3/25
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class FundListReq extends BaseReq {

    /**
     * 页码
     */
    private int pageNumber;

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * 页数
     */
    private int pageSize;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
