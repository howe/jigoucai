package com.jigoucai.bean.param.req.order;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/3/20
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class InfoReq extends BaseReq {

    /**
     * 合作商订单号
     */
    private String outOrderNum;

    public String getOutOrderNum() {
        return outOrderNum;
    }

    public void setOutOrderNum(String outOrderNum) {
        this.outOrderNum = outOrderNum;
    }
}
