package com.jigoucai.bean.param.req.order;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/3/26
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class DetailReq extends BaseReq {

    private String pOrderNum;

    public String getpOrderNum() {
        return pOrderNum;
    }

    public void setpOrderNum(String pOrderNum) {
        this.pOrderNum = pOrderNum;
    }
}
