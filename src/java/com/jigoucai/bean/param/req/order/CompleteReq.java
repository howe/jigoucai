package com.jigoucai.bean.param.req.order;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created by Jianghao on 2018/3/28
 *
 * @howechiang
 */
public class CompleteReq extends BaseReq {

    private String pOrderNum;

    public String getpOrderNum() {
        return pOrderNum;
    }

    public void setpOrderNum(String pOrderNum) {
        this.pOrderNum = pOrderNum;
    }

    private String filePath1;

    public String getFilePath1() {
        return filePath1;
    }

    public void setFilePath1(String filePath1) {
        this.filePath1 = filePath1;
    }

    private String filePath2;

    public String getFilePath2() {
        return filePath2;
    }

    public void setFilePath2(String filePath2) {
        this.filePath2 = filePath2;
    }

    private String filePath3;

    public String getFilePath3() {
        return filePath3;
    }

    public void setFilePath3(String filePath3) {
        this.filePath3 = filePath3;
    }
}
