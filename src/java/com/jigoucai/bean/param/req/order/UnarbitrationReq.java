package com.jigoucai.bean.param.req.order;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created by Jianghao on 2018/3/28
 *
 * @howechiang
 */
public class UnarbitrationReq extends BaseReq {

    private String pOrderNum;

    public String getpOrderNum() {
        return pOrderNum;
    }

    public void setpOrderNum(String pOrderNum) {
        this.pOrderNum = pOrderNum;
    }

}
