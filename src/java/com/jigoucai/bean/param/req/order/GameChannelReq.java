package com.jigoucai.bean.param.req.order;

import com.jigoucai.bean.param.req.BaseReq;

/**
 * Created on 2018/4/2
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class GameChannelReq extends BaseReq {

    private int gameId;

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }
}
