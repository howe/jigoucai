package com.jigoucai.bean.param.biz;

/**
 * Created on 2018/3/18
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class MLogin extends UserInfo {

    private long failureTime;

    public long getFailureTime() {
        return failureTime;
    }

    public void setFailureTime(long failureTime) {
        this.failureTime = failureTime;
    }

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public MLogin(long failureTime, String token, int userId) {
        this.failureTime = failureTime;
        this.token = token;
        this.userId = userId;
    }

    public MLogin() {
    }
}
