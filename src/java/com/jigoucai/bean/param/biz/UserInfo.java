package com.jigoucai.bean.param.biz;

import com.jigoucai.entity.user.User;

/**
 * Created on 2018/3/15
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class UserInfo {

    /**
     * 昵称
     */
    private String nickname;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * 用户ID
     */
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * 用户名
     */
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 绑定手机
     */
    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 是否实名
     */
    private boolean isAuth;

    public boolean isAuth() {
        return isAuth;
    }

    public void setAuth(boolean auth) {
        isAuth = auth;
    }

    /**
     * 头像地址
     */
    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    /**
     * 可用余额
     */
    private double myFund;

    public double getMyFund() {
        return myFund;
    }

    public void setMyFund(double myFund) {
        this.myFund = myFund;
    }

    /**
     * 保证金余额
     */
    private double myMargin;

    public double getMyMargin() {
        return myMargin;
    }

    public void setMyMargin(double myMargin) {
        this.myMargin = myMargin;
    }

    public UserInfo(User user) {
        this.nickname = user.getNickName();
        this.userId = user.getUserId();
        this.username = user.getUsername();
        this.mobile = user.getMobile();
        this.isAuth = user.isAuth();
        this.avatarUrl = user.getAvatarUrl();
    }

    public UserInfo() {
    }
}
