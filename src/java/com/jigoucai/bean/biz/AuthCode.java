package com.jigoucai.bean.biz;

import com.jigoucai.bean.dict.Dict;
import com.jigoucai.entity.BaseEntity;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.json.JsonField;

/**
 *
 */
public class AuthCode{

    /**
     * 接收安全的对象类型。有以下值之一：PHONE；EMAIL。
     */
    private String targetType;

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    /**
     * 接收安全码的对象。一般为手机号码或邮件地址。
     */
    private String target;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * 关联的用户编号。如果为空，一般表示该用户还没有创建，正在进行注册过程。
     */
    private Integer userId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * 该安全码的用途。有以下值之一：
     * REG：注册；
     * RESET_PWD：重置密码；
     * BIND：绑定；
     * UNBIND：取消绑定；
     * LOGIN：登录；
     * 等。
     * 根据业务需要可以扩充定义。每个代码都对应一个具体业务用途。
     */
    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    /**
     * 安全码。
     * 不同 target_type 的验证码的生成规则会不一样，生成的验证码的长度也会不一样。
     * 比如手机短信验证一般4位数字即可，邮件验证码会是较长的一段随机字符串（如 uuid）。
     */
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 该安全码的状态。有以下值之一：
     * SENT：发送成功；
     * CONSUMED：已使用；
     * ABORTED：被作废；
     * UNSENT：未发生成功；
     */
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 过期时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL)
    private java.util.Date expireTime;

    public java.util.Date getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(java.util.Date expireTime) {
        this.expireTime = expireTime;
    }

    /**
     * 创建时间
     */
    @JsonField(dataFormat = Dict.DATE_FORMART_FULL)
    private java.util.Date createTime;

    public java.util.Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "AuthCode{" +
                "targetType='" + targetType + '\'' +
                ", target='" + target + '\'' +
                ", userId=" + userId +
                ", action='" + action + '\'' +
                ", code='" + code + '\'' +
                ", status='" + status + '\'' +
                ", expireTime=" + expireTime +
                ", createTime=" + createTime +
                '}';
    }
}