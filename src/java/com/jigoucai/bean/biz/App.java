package com.jigoucai.bean.biz;

/**
 * Created on 2018/3/14
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class App {

    /**
     * APP名
     */
    private String appName;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    private String appVersion;

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    /**
     * 渠道
     */
    private String channel;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
