package com.jigoucai.bean.biz;

/**
 * Created by Jianghao on 2018/2/2
 *
 * @howechiang
 */
public class CashAccount {

    /**
     * 账户名
     */
    private String accountName;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    /**
     * 开户行
     */
    private String bankName;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * 账户
     */
    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 是否默认
     */
    private boolean isDefault;

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        this.isDefault = aDefault;
    }

    public CashAccount(String accountName, String bankName, String account) {
        this.accountName = accountName;
        this.bankName = bankName;
        this.account = account;
    }

    public CashAccount(String accountName, String bankName, String account, boolean isDefault) {
        this.accountName = accountName;
        this.bankName = bankName;
        this.account = account;
        this.isDefault = isDefault;
    }

    public CashAccount() {
    }
}
