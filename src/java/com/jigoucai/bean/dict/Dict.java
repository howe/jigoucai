package com.jigoucai.bean.dict;

import com.jigoucai.bean.biz.CashAccount;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class Dict {

    public static final String DATE_FORMART_FULL = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_CHINA_TIMEZONE = "GMT+8";

    public static final String ADMINISTRATIVE_REGIONS_NATIONAL = "0086";
    public static final String ADMINISTRATIVE_REGIONS_SHANGHAI = "310000";
    public static final String ADMINISTRATIVE_REGIONS_YUNNAN = "530000";
    public static final String ADMINISTRATIVE_REGIONS_NEIMENGGU = "150000";
    public static final String ADMINISTRATIVE_REGIONS_BEIJING = "110000";
    public static final String ADMINISTRATIVE_REGIONS_JILIN = "220000";
    public static final String ADMINISTRATIVE_REGIONS_SICHUAN = "510000";
    public static final String ADMINISTRATIVE_REGIONS_TIANJIN = "120000";
    public static final String ADMINISTRATIVE_REGIONS_NINGXIA = "640000";
    public static final String ADMINISTRATIVE_REGIONS_ANHUI = "340000";
    public static final String ADMINISTRATIVE_REGIONS_SHANDONG = "370000";
    /**
     * 山西
     */
    public static final String ADMINISTRATIVE_REGIONS_SHANXI = "140000";
    public static final String ADMINISTRATIVE_REGIONS_GUANGDONG = "440000";
    public static final String ADMINISTRATIVE_REGIONS_GUANGXI = "450000";
    public static final String ADMINISTRATIVE_REGIONS_XINJIANG = "650000";
    public static final String ADMINISTRATIVE_REGIONS_JIANGSU = "320000";
    public static final String ADMINISTRATIVE_REGIONS_JIANGXI = "360000";
    public static final String ADMINISTRATIVE_REGIONS_HEBEI = "130000";
    public static final String ADMINISTRATIVE_REGIONS_HENAN = "410000";
    public static final String ADMINISTRATIVE_REGIONS_ZHEJIANG = "330000";
    public static final String ADMINISTRATIVE_REGIONS_HAINAN = "460000";
    public static final String ADMINISTRATIVE_REGIONS_HUBEI = "420000";
    public static final String ADMINISTRATIVE_REGIONS_HUNAN = "430000";
    public static final String ADMINISTRATIVE_REGIONS_GANSU = "620000";
    public static final String ADMINISTRATIVE_REGIONS_FUJIAN = "350000";
    public static final String ADMINISTRATIVE_REGIONS_XICANG = "540000";
    public static final String ADMINISTRATIVE_REGIONS_GUIZHOU = "520000";
    public static final String ADMINISTRATIVE_REGIONS_LIAONING = "210000";
    public static final String ADMINISTRATIVE_REGIONS_ZHONGQING = "500000";
    /**
     * 陕西
     */
    public static final String ADMINISTRATIVE_REGIONS_SHAANXI = "610000";
    public static final String ADMINISTRATIVE_REGIONS_QINGHAI = "630000";
    public static final String ADMINISTRATIVE_REGIONS_HEILONGJIANG = "230000";

    public static Map<String, String> pro_map_name() {

        Map<String, String> map = new HashMap<>();
        map.put("0086", "全国");
        map.put("310000", "上海");
        map.put("530000", "云南");
        map.put("150000", "内蒙古");
        map.put("110000", "北京");
        map.put("220000", "吉林");
        map.put("510000", "四川");
        map.put("120000", "天津");
        map.put("640000", "宁夏");
        map.put("340000", "安徽");
        map.put("370000", "山东");
        map.put("140000", "山西");
        map.put("440000", "广东");
        map.put("450000", "广西");
        map.put("650000", "新疆");
        map.put("320000", "江苏");
        map.put("360000", "江西");
        map.put("130000", "河北");
        map.put("410000", "河南");
        map.put("330000", "浙江");
        map.put("460000", "海南");
        map.put("420000", "湖北");
        map.put("430000", "湖南");
        map.put("620000", "甘肃");
        map.put("350000", "福建");
        map.put("540000", "西藏");
        map.put("520000", "贵州");
        map.put("210000", "辽宁");
        map.put("500000", "重庆");
        map.put("610000", "陕西");
        map.put("630000", "青海");
        map.put("230000", "黑龙江");
        return map;
    }

    public static Map<String, String> pro_map_code() {

        Map<String, String> map = new HashMap<>();
        map.put("全国", "0086");
        map.put("上海", "310000");
        map.put("云南", "530000");
        map.put("内蒙古", "150000");
        map.put("北京", "110000");
        map.put("吉林", "220000");
        map.put("四川", "510000");
        map.put("天津", "120000");
        map.put("宁夏", "640000");
        map.put("安徽", "340000");
        map.put("山东", "370000");
        map.put("山西", "140000");
        map.put("广东", "440000");
        map.put("广西", "450000");
        map.put("新疆", "650000");
        map.put("江苏", "320000");
        map.put("江西", "360000");
        map.put("河北", "130000");
        map.put("河南", "410000");
        map.put("浙江", "330000");
        map.put("海南", "460000");
        map.put("湖北", "420000");
        map.put("湖南", "430000");
        map.put("甘肃", "620000");
        map.put("福建", "350000");
        map.put("西藏", "540000");
        map.put("贵州", "520000");
        map.put("辽宁", "210000");
        map.put("重庆", "500000");
        map.put("陕西", "610000");
        map.put("青海", "630000");
        map.put("黑龙江", "230000");
        return map;
    }

    /**
     * 中国移动
     */
    public static final String OPERATORS_CMCC = "1";
    /**
     * 中国联通
     */
    public static final String OPERATORS_CUCC = "2";
    /**
     * 中国电信
     */
    public static final String OPERATORS_CTCC = "3";

    public static Map<String, String> isp_map_code() {

        Map<String, String> map = new HashMap<>();
        map.put("中国移动", "1");
        map.put("中国联通", "2");
        map.put("中国电信", "3");
        return map;
    }

    public static Map<String, String> isp_map_name() {

        Map<String, String> map = new HashMap<>();
        map.put("1", "中国移动");
        map.put("2", "中国联通");
        map.put("3", "中国电信");
        return map;
    }

    public static final String ACTION_USER_LOGIN_PATH = "/user/login.htm";
    public static final String ACTION_USER_SLOGIN_PATH = "/user/sLogin.htm";
    public static final String ACTION_USER_MLOGIN_PATH = "/user/mLogin.htm";
    public static final String ACTION_USER_SENDCODE_PATH = "/user/sendCode.htm";
    public static final String ACTION_USER_HALL_MENU = "/hall/menu.htm";
    public static final String ACTION_USER_HALL_INDEX = "/hall/index.htm";

    public static final String ACTION_PAYMENT_WEIXIN_NOTIFY = "/payment/weixin/notify.htm";
    public static final String ACTION_CMS_ABOUT = "/cms/about.htm";
    public static final String ACTION_IMAGES_UPLOAD = "/images/upload.htm";
    public static final String ACTION_CMS_QALIST = "/cms/qaList.htm";
    public static final String USER_TOKEN_NAME = "token";

    public static final String PARAM_VERSION_NAME = "version";

    public static final String PARAM_TIMESTAMP_NAME = "timestamp";

    public static final String PARAM_USERID_NAME = "userId";

    public static final String API_VERSION = "1.0";

    public static final int DOCKING_TIME = 300;

    /**
     * 支付宝交易状态
     */
    public static final String ALIPAY_TRADE_STATUS_WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
    public static final String ALIPAY_TRADE_STATUS_TRADE_CLOSED = "TRADE_CLOSED";
    public static final String ALIPAY_TRADE_STATUS_TRADE_SUCCESS = "TRADE_SUCCESS";
    public static final String ALIPAY_TRADE_STATUS_TRADE_FINISHED = "TRADE_FINISHED";

    public static final String BL_TRUE = "true";
    public static final String BL_FLASE = "flase";
    public static final String RTN_SUCCESS = "success";
    public static final String RTN_FAILED = "failed";
    public static final String CHARSET_UTF8 = "utf-8";
    public static final String FORMAT_JSON = "JSON";
    public static final String SIGN_TYPE_RSA2 = "RSA2";
    public static final String SIGN_TYPE_MD5 = "MD5";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_GET = "GET";
    public static final String FEE_TYPE_CNY = "CNY";

    /**
     * 订单类型
     * 1 普通
     * 2 优质
     * 3 私有
     */
    public static final Integer ORDERTYPE_ARRAY[] = {1, 2, 3};

    public static final Integer ORDERTYPE_ORDINARY = 1;
    public static final Integer ORDERTYPE_GRADE = 2;
    public static final Integer ORDERTYPE_PRIVATE = 3;

    /**
     * 代练(Power Leveling)类型
     * 1 排位 默认
     * 2 定位
     * 3 等级
     * 4 陪练
     * 5 匹配
     * 6 晋级
     * 7 级别
     * 0 其他
     */
    public static final Integer PLTYPE_ARRAY[] = {0, 1, 2, 3, 4, 5, 6, 7};

    /**
     * 订单状态
     * 0 已取消
     * 1 已下单（未接手）
     * 2 已接单（未处理）
     * 3 处理中（代练中）
     * 4 待验收
     * 5 主动撤销中
     * 6 仲裁中
     * 7 异常
     * 8 锁定
     * 9 已下架
     * 10 已撤销
     * 11 已仲裁
     * 12 强制仲裁
     * 13 申述中（服务商申述）
     * 14 已完成
     * 15 被动撤销中
     * 88 已结算
     */
    public static final Integer ORDERSTATUS_ARRAY[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 88};

    /**
     * 已取消
     */
    public static final Integer ORDERSTATUS_CANCELLED = 0;

    /**
     * 已下单（未接手）
     */
    public static final Integer ORDERSTATUS_ALREADY_ORDERED = 1;

    /**
     * 已接手（未处理）
     */
    public static final Integer ORDERSTATUS_ACCEPTED = 2;

    /**
     * 处理中（代练中）
     */
    public static final Integer ORDERSTATUS_PROCESSING = 3;

    /**
     * 待验收
     */
    public static final Integer ORDERSTATUS_TO_BE_ACCEPTED = 4;

    /**
     * 主动撤销中
     */
    public static final Integer ORDERSTATUS_REVOKING = 5;

    /**
     * 仲裁中
     */
    public static final Integer ORDERSTATUS_ARBITRATION = 6;

    /**
     * 异常
     */
    public static final Integer ORDERSTATUS_EXCEPTION = 7;

    /**
     * 锁定
     */
    public static final Integer ORDERSTATUS_LOCKED = 8;

    /**
     * 已下架
     */
    public static final Integer ORDERSTATUS_OFF_SHELF = 9;

    /**
     * 已撤销
     */
    public static final Integer ORDERSTATUS_REVOKED = 10;

    /**
     * 已仲裁
     */
    public static final Integer ORDERSTATUS_ARBITRATED = 11;

    /**
     * 强制仲裁
     */
    public static final Integer ORDERSTATUS_MANDATORY_ARBITRATION = 12;

    /**
     * 申述中
     */
    public static final Integer ORDERSTATUS_REPRESENTATION = 13;

    /**
     * 已完成
     */
    public static final Integer ORDERSTATUS_COMPLETED = 14;

    /**
     * 被动撤销中
     */
    public static final Integer ORDERSTATUS_BCOMPLETED = 15;

    /**
     * 已结算
     */
    public static final Integer ORDERSTATUS_SETTLED = 88;

    public static final String ACTION_GAME_QUERYGAMELIST = "/game/queryGameList.htm";

    public static final String OPERATIONTYPE_ARRAY[] = {"OP_LOCK", "OP_UNLOCK",
            "OP_APPLY_ACCEPTANCE", "OP_CANCEL_ACCEPTANCE", "OP_COMPLETE_ACCEPTANCE",
            "OP_APPLY_CANCELLATION", "OP_CANCELL_CANCELLATION", "OP_AGREE_WITHDRAW",
            "OP_APPLY_ARBITRATION", "OP_CANCEL_ARBITRATION", "OP_ABNORMAL_EXCEPTION",
            "OP_CANCEL_EXCEPTION", "OP_STOPPAGE_TIME", "OP_FILLING_MONEY", "OP_CORRECT_PASSWORD",
            "OPERATIONTYPE_OP_ENFORCE_REVOKE", "OPERATIONTYPE_OP_KEFU_ARBITRATION"};
    /**
     * 锁定账号
     */
    public static final String OPERATIONTYPE_OP_LOCK = "OP_LOCK";
    /**
     * 取消锁定
     */
    public static final String OPERATIONTYPE_OP_UNLOCK = "OP_UNLOCK";
    /**
     * 申请验收
     */
    public static final String OPERATIONTYPE_OP_APPLY_ACCEPTANCE = "OP_APPLY_ACCEPTANCE";
    /**
     * 取消验收
     */
    public static final String OPERATIONTYPE_OP_CANCEL_ACCEPTANCE = "OP_CANCEL_ACCEPTANCE";
    /**
     * 完成验收
     */
    public static final String OPERATIONTYPE_OP_COMPLETE_ACCEPTANCE = "OP_COMPLETE_ACCEPTANCE";
    /**
     * 申请撤销
     */
    public static final String OPERATIONTYPE_OP_APPLY_REVOKE = "OP_APPLY_REVOKE";
    /**
     * 取消撤销
     */
    public static final String OPERATIONTYPE_OP_CANCEL_REVOKE = "OP_CANCEL_REVOKE";
    /**
     * 同意撤销
     */
    public static final String OPERATIONTYPE_OP_AGREE_REVOKE = "OP_AGREE_REVOKE";
    /**
     * 申请仲裁
     */
    public static final String OPERATIONTYPE_OP_APPLY_ARBITRATION = "OP_APPLY_ARBITRATION";
    /**
     * 取消仲裁
     */
    public static final String OPERATIONTYPE_OP_CANCEL_ARBITRATION = "OP_CANCEL_ARBITRATION";
    /**
     * 客服仲裁
     */
    public static final String OPERATIONTYPE_OP_KEFU_ARBITRATION = "OP_KEFU_ARBITRATION";
    /**
     * 强制撤销
     */
    public static final String OPERATIONTYPE_OP_ENFORCE_REVOKE = "OP_ENFORCE_REVOKE";
    /**
     * 提交异常
     */
    public static final String OPERATIONTYPE_OP_ABNORMAL_EXCEPTION = "OP_ABNORMAL_EXCEPTION";
    /**
     * 取消异常
     */
    public static final String OPERATIONTYPE_OP_CANCEL_EXCEPTION = "OP_CANCEL_EXCEPTION";
    /**
     * 补时
     */
    public static final String OPERATIONTYPE_OP_ADD_TIME = "OP_ADD_TIME";
    /**
     * 补款
     */
    public static final String OPERATIONTYPE_OP_ADD_MONEY = "OP_ADD_MONEY";
    /**
     * 修正密码
     */
    public static final String OPERATIONTYPE_OP_CORRECT_PASSWORD = "OP_CORRECT_PASSWORD";

    /**
     * 事项
     * 1 缴纳
     * 2 冻结
     * 3 赔付
     * 4 解冻
     * 5 释放（转余额）
     */
    public static final int FINANCE_MARGIN_CAUSE_PAY = 1;
    public static final int FINANCE_MARGIN_CAUSE_FREEZE = 2;
    public static final int FINANCE_MARGIN_CAUSE_PAYOUT = 3;
    public static final int FINANCE_MARGIN_CAUSE_THAW = 4;
    public static final int FINANCE_MARGIN_CAUSE_FREED = 5;

    /**
     * 默认支付宝收款账户
     */
    public static final CashAccount JDG_DEFAULT_RECEIVING_ACCOUNT_ALIPAY = new CashAccount("南京金游信息科技有限公司", "微信支付", "1499732362");

    /**
     * 默认支付宝付款账户
     */
    public static final CashAccount JDG_DEFAULT_PAYMENT_ACCOUNT_ALIPAY = new CashAccount("南京金游信息科技有限公司", "微信支付", "1499732362");

}
