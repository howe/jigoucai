package com.jigoucai;

import org.nutz.boot.NbApp;
import org.nutz.mvc.annotation.*;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang@gmail.com)
 */
@Fail("http:500")
@Encoding(input = "utf8", output = "utf8")
@ChainBy(args = "mvc/nutz-mvc-chain.js")
@SetupBy(value = MainSetup.class)
public class MainLauncher {

    public static void main(String[] args) {
        new NbApp(MainLauncher.class).run();
    }
}