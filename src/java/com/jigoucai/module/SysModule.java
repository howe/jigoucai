package com.jigoucai.module;

import com.jigoucai.bean.param.req.cms.AboutReq;
import com.jigoucai.bean.param.req.cms.BannerListReq;
import com.jigoucai.bean.param.req.cms.CollectFormIdReq;
import com.jigoucai.bean.param.req.cms.QaListReq;
import com.jigoucai.bean.param.resp.BaseResp;
import com.jigoucai.entity.cms.Qa;
import com.jigoucai.entity.hall.Banner;
import com.jigoucai.entity.hall.Marquee;
import com.jigoucai.entity.user.Account;
import com.jigoucai.util.Code;
import com.jigoucai.util.JgcUtil;
import com.jigoucai.util.Util;
import org.nutz.dao.Cnd;
import org.nutz.dao.pager.Pager;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Lang;
import org.nutz.lang.Times;
import org.nutz.lang.util.NutMap;
import org.nutz.mvc.adaptor.JsonAdaptor;
import org.nutz.mvc.annotation.*;
import java.util.List;

/**
 * Created on 2018/3/25
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
@IocBean
@Fail("http:500")
public class SysModule extends BaseModule {

    /**
     * 获取大厅banner列表
     *
     * @param req
     * @return
     */
    @POST
    @At("/cms/bannerList")
    @Ok("json:full")
    @AdaptBy(type = JsonAdaptor.class)
    public BaseResp bannerList(BannerListReq req) {
        try {
            Pager pager = Util.defaultPager(1, 5);
            List<Banner> list = dao.query(Banner.class, Cnd.where("isDisplay", "=", true).desc("addTime"), pager);
            return JgcUtil.buildSuccessResp(new NutMap().setv("list", list));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Code.resp("SYS_ERROR");
        }
    }

    /**
     * 大厅跑马灯
     *
     * @param req
     * @return
     */
    @POST
    @At("/cms/marquee")
    @Ok("json:full")
    @AdaptBy(type = JsonAdaptor.class)
    public BaseResp marquee(BannerListReq req) {
        try {
            Pager pager = Util.defaultPager(1, 5);
            List<Marquee> list = dao.query(Marquee.class, Cnd.orderBy().desc("pushTime"), pager);
            return JgcUtil.buildSuccessResp(new NutMap().setv("list", list));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Code.resp("SYS_ERROR");
        }
    }

    /**
     * 关于信息
     *
     * @param req
     * @return
     */
    @POST
    @At("/cms/about")
    @Ok("json:full")
    @AdaptBy(type = JsonAdaptor.class)
    public BaseResp about(AboutReq req) {
        try {
            List<com.jigoucai.entity.cms.Param> params = dao.query(com.jigoucai.entity.cms.Param.class, Cnd.where("isUse", "=", true));
            NutMap map = new NutMap();
            params.stream().forEach(param -> map.setv(param.getKey(), param.getValue()));
            return JgcUtil.buildSuccessResp(new NutMap().setv("appVersion", map.getString("appVersion"))
                    .setv("serviceHotline", map.getString("serviceHotline")).setv("serviceEmail", map.getString("serviceEmail"))
                    .setv("appName", map.getString("appName")).setv("companyName", map.getString("companyName"))
                    .setv("serviceQQ", map.getString("serviceQQ")).setv("serviceQun", map.getString("serviceQun")));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Code.resp("SYS_ERROR");
        }
    }

    /**
     * 帮助列表
     *
     * @param req
     * @return
     */
    @POST
    @At("/cms/qaList")
    @Ok("json:full")
    @AdaptBy(type = JsonAdaptor.class)
    public BaseResp qaList(QaListReq req) {
        try {
            List<Qa> list = dao.query(Qa.class, Cnd.where("isDisplay", "=", true).desc("releaseTime"));
            return JgcUtil.buildSuccessResp(new NutMap().setv("list", list));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Code.resp("SYS_ERROR");
        }
    }

    /**
     * 收集formId
     *
     * @param req
     * @return
     */
    @POST
    @At("/sys/collectFormId")
    @Ok("json:full")
    @AdaptBy(type = JsonAdaptor.class)
    public BaseResp collectFormId(CollectFormIdReq req) {

        try {
            Account account = dao.fetch(Account.class, Cnd.where("userId", "=", req.getUserId()).and("source", "=", "MINIAPP"));
            if (Lang.isEmpty(account)) {
                return Code.resp("USER_NOT_FOUND");
            } else {
                NutMap map = new NutMap();
                if (!Lang.isEmpty(account.getReserve())) {
                    map.setAll(account.getReserve());
                }
                map.setv(Times.getTS() + "", req.getFormId());
                account.setReserve(map);
                dao.update(account, "^(reserve)$");
                return JgcUtil.buildSuccessResp("");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Code.resp("SYS_ERROR");
        }
    }
}
