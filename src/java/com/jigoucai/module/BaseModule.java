package com.jigoucai.module;

import com.jigoucai.service.*;
import org.nutz.dao.Condition;
import org.nutz.dao.Dao;
import org.nutz.dao.QueryResult;
import org.nutz.dao.pager.Pager;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.util.NutMap;
import org.nutz.log.Log;
import org.nutz.log.Logs;

import java.util.List;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
@IocBean
public abstract class BaseModule {

    @Inject
    protected Dao dao;

    @Inject
    protected Dao dao2;

    @Inject
    OrderService orderService;

    @Inject
    UserService userService;

    @Inject
    FinanceService financeService;

    protected static final Log log = Logs.get();

    protected QueryResult query(Class<?> klass, Condition cnd, Pager pager) {
        if (pager != null && pager.getPageNumber() < 1) {
            pager.setPageNumber(1);
        }
        List<?> list = dao.query(klass, cnd, pager);
        dao.fetchLinks(list, null);
        pager.setRecordCount(dao.count(klass, cnd));
        return new QueryResult(list, pager);
    }

    protected NutMap ajaxOk(Object data) {
        return new NutMap().setv("ok", true).setv("data", data);
    }

    protected NutMap ajaxFail(String msg) {
        return new NutMap().setv("ok", false).setv("msg", msg);
    }
}
