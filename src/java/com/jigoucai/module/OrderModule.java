package com.jigoucai.module;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.lang.util.NutMap;
import org.nutz.mvc.adaptor.JsonAdaptor;
import org.nutz.mvc.annotation.*;
import org.nutz.weixin.bean.mp.biz.WXMsgData;
import org.nutz.weixin.bean.mp.req.MessageWxopenTemplateSendReq;
import org.nutz.weixin.util.mp.MpUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 订单
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
@IocBean
@Fail("http:500")
@At("/order")
public class OrderModule extends BaseModule {


}
