package com.jigoucai.service;

import org.nutz.integration.jedis.RedisService;
import org.nutz.ioc.aop.Aop;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Lang;
import org.nutz.lang.Times;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.Mvcs;
import redis.clients.jedis.Jedis;

import java.util.Date;
import java.util.Set;

/**
 * Created by Jianghao on 2017/9/6
 *
 * @howechiang
 */
@IocBean
public class JedisService {

    protected static final Log log = Logs.get();
    protected static Jedis jedis;

    public static Jedis jedis() {
        if (jedis == null) {
            jedis = Mvcs.ctx().getDefaultIoc().get(RedisService.class);
            return jedis;
        }
        return jedis;
    }

    public void setJedis(Jedis jedis) {
        this.jedis = jedis;
    }

    @Aop("redis")
    public static void set(String key, String val, Integer sec) {

        try {
            jedis().del(key);
            jedis().set(key, val);
            if (!Lang.isEmpty(sec)) {
                jedis().expireAt(key, Times.nextSecond(Times.now(), sec).getTime() / 1000);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Aop("redis")
    public static void set(String key, String val) {

        try {
            jedis().del(key);
            jedis().set(key, val);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Aop("redis")
    public static void set(String key, String val, Date date) {

        try {
            jedis().del(key);
            jedis().set(key, val);
            if (!Lang.isEmpty(date)) {
                jedis().expireAt(key, date.getTime() / 1000);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Aop("redis")
    public static void del(String key) {
        try {
            jedis().del(key);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Aop("redis")
    public static void expire(String key, Integer sec) {
        try {
            jedis().expireAt(key, Times.nextSecond(Times.now(), sec).getTime() / 1000);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Aop("redis")
    public static void expire(String key, Date date, Integer sec) {
        try {
            jedis().expireAt(key, Times.nextSecond(date, sec).getTime() / 1000);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Aop("redis")
    public static String get(String key) {
        try {
            if (exists(key)) {
                return jedis().get(key);
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @Aop("redis")
    public static Set<String> keys(String key) {
        try {
            return jedis().keys(key);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @Aop("redis")
    public static Boolean exists(String key) {
        return jedis().exists(key);
    }

    @Aop("redis")
    public static Boolean isExist(String key) {

        if (Lang.isEmpty(keys(key))) {
            return false;
        } else {
            return true;
        }
    }

    @Aop("redis")
    public static String query(String key) {
        try {
            if (isExist(key)) {
                return get(keys(key).iterator().next());
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @Aop("redis")
    public static void update(String key, String val) {

        try {
            jedis().del(key);
            jedis().set(key, val);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Aop("redis")
    public static void update(String key, String key1, String val) {

        try {
            jedis().del(key);
            jedis().set(key1, val);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Aop("redis")
    public static void rename(String key, String key1) {

        try {
            jedis().rename(key, key1);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Aop("redis")
    public static Long publish(String channel, String message) {

        try {
            return jedis().publish(channel, message);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
