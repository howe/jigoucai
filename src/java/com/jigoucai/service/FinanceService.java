package com.jigoucai.service;

import com.jigoucai.entity.finance.Current;
import com.jigoucai.entity.finance.Fund;
import org.nutz.dao.pager.Pager;

import java.util.List;

public interface FinanceService {

    /**
     * 资金流水
     * @param userId
     * @param pager
     * @return
     */
    List<Fund> queryFund(Integer userId, Pager pager);

    /**
     * 往来帐流水
     * @param userId
     * @param pager
     * @return
     */
    List<Current> queryCurrent(Integer userId, Pager pager);

    /**
     * 查看保证金余额
     * @param userId
     * @return
     */
    double getMargin(Integer userId);

    /**
     * 查看资金余额
     * @param userId
     * @return
     */
    double getFund(Integer userId);
}
