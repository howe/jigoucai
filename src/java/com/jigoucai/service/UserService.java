package com.jigoucai.service;

import com.jigoucai.bean.param.biz.MLogin;
import com.jigoucai.bean.param.req.user.MLoginReq;
import com.jigoucai.entity.user.Account;
import com.jigoucai.entity.user.Token;
import com.jigoucai.entity.user.User;

import javax.servlet.http.HttpServletRequest;

/**
 * Created on 2018/3/17
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public interface UserService {

    /**
     * @param source 登录来源
     * @param openId OPENID
     * @return
     */
    Account fetch(String source, String openId);

    /**
     * @param userId 用户ID
     * @return
     */
    User fetch(Integer userId);

    /**
     * @param req
     * @param request
     * @return
     */
    MLogin mLogin(MLoginReq req, HttpServletRequest request);

    /**
     * @param userId 用户ID
     * @return
     */
    Token fetchToken(Integer userId);
}
