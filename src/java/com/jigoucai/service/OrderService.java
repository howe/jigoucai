package com.jigoucai.service;

import com.jigoucai.entity.order.Order;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang@gmail.com)
 */
public interface OrderService {

    /**
     * ID查询
     * @param orderId
     * @return
     */
    Order fetch(Integer orderId);

    /**
     * 订单编号查询
     * @param orderNum
     * @return
     */
    Order fetch(String orderNum);

    /**
     * 添加订单
     * @param order
     * @return
     */
    Order insert(Order order);

    /**
     * 更新订单
     * @param order
     * @param actived
     * @return
     */
    int update(Order order, String actived);

    /**
     * 更新订单
     * @param order
     * @return
     */
    int update(Order order);

    /**
     * 删除订单
     * @param orderId
     * @return
     */
    int delete(Integer orderId);

    /**
     * 删除订单
     * @param orderNum
     * @return
     */
    int delete(String orderNum);
}
