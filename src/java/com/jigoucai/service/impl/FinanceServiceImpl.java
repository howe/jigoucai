package com.jigoucai.service.impl;

import com.jigoucai.entity.finance.Current;
import com.jigoucai.entity.finance.Fund;
import com.jigoucai.service.FinanceService;
import org.nutz.dao.Cnd;
import org.nutz.dao.Sqls;
import org.nutz.dao.pager.Pager;
import org.nutz.dao.sql.Sql;
import org.nutz.ioc.loader.annotation.IocBean;

import java.util.List;

@IocBean
public class FinanceServiceImpl extends BaseServiceImpl implements FinanceService {
    @Override
    public List<Fund> queryFund(Integer userId, Pager pager) {
        return dao.query(Fund.class, Cnd.where("userId", "=", userId), pager);
    }

    @Override
    public List<Current> queryCurrent(Integer userId, Pager pager) {
        return dao.query(Current.class, Cnd.where("userId", "=", userId), pager);
    }

    @Override
    public double getMargin(Integer userId) {
        Sql sql = Sqls.create("SELECT IFNULL(SUM(amount),0) margin FROM finance_margin WHERE user_id = @USERID");
        sql.params().set("USERID", userId);
        sql.setCallback(Sqls.callback.doubleValue());
        dao.execute(sql);
        return sql.getDouble();
    }

    @Override
    public double getFund(Integer userId) {
        Sql sql = Sqls.create("SELECT IFNULL(SUM(amount),0) fund FROM finance_fund WHERE user_id = @USERID");
        sql.params().set("USERID", userId);
        sql.setCallback(Sqls.callback.doubleValue());
        dao.execute(sql);
        return sql.getDouble();
    }
}
