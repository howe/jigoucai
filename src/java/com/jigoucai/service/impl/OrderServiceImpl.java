package com.jigoucai.service.impl;

import com.jigoucai.entity.order.Order;
import com.jigoucai.service.OrderService;
import org.nutz.ioc.loader.annotation.IocBean;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang@gmail.com)
 */
@IocBean
public class OrderServiceImpl extends BaseServiceImpl implements OrderService {

    @Override
    public Order fetch(Integer orderId) {
        return dao.fetchLinks(dao.fetch(Order.class, orderId), null);
    }

    @Override
    public Order fetch(String orderNum) {
        return dao.fetchLinks(dao.fetch(Order.class, orderNum), null);
    }

    @Override
    public Order insert(Order order) {
        return null;
    }

    @Override
    public int update(Order order, String actived) {
        return dao.update(order, actived);
    }

    @Override
    public int update(Order order) {
        return dao.update(order);
    }

    @Override
    public int delete(Integer orderId) {
        return dao.delete(orderId);
    }

    @Override
    public int delete(String orderNum) {
        return dao.delete(orderNum);
    }
}
