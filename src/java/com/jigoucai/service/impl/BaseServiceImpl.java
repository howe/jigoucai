package com.jigoucai.service.impl;

import org.nutz.dao.Dao;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.log.Log;
import org.nutz.log.Logs;
import org.nutz.mvc.Mvcs;

/**
 * Created on 2017/11/19
 *
 * @author Jianghao(howechiang@gmail.com)
 */
@IocBean
public class BaseServiceImpl {

    protected static final Log log = Logs.get();

    /**
     * APP库
     */
    @Inject
    protected Dao dao;

    /**
     * BS库
     */
    @Inject
    protected Dao dao2;
}
