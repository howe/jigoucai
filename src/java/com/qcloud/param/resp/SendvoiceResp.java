package com.qcloud.param.resp;

/**
 * Created on 2018/1/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class SendvoiceResp {

    /**
     * 0表示成功，非0表示失败
     */
    private int result;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    /**
     * result非0时的具体错误信息
     */
    private String errmsg;

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    /**
     * 用户的session内容，腾讯server回包中会原样返回
     */
    private String ext;

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    /**
     * 标识本次发送id，标识一次下发记录
     */
    private String callid;

    public String getCallid() {
        return callid;
    }

    public void setCallid(String callid) {
        this.callid = callid;
    }
}
