package com.qcloud.param.resp;

/**
 * Created on 2018/1/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class SendismsResp extends SendsmsResp {

    /**
     * 根据请求包体中tel字段而解析出的国家码
     */
    private String nationcode;

    public String getNationcode() {
        return nationcode;
    }

    public void setNationcode(String nationcode) {
        this.nationcode = nationcode;
    }
}
