package com.qcloud.param.req;

import com.qcloud.param.biz.Tel;

/**
 * Created on 2018/1/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class SendvoiceReq {

    /**
     * 电话号码
     */
    private Tel tel;

    public Tel getTel() {
        return tel;
    }

    public void setTel(Tel tel) {
        this.tel = tel;
    }

    /**
     * 验证码，支持英文字母、数字及组合，实际发送给用户时，语音验证码内容前会添加"您的验证码是"语音提示
     */
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * 	播放次数，最多 3 次，默认 2 次
     */
    private int playtimes;

    public int getPlaytimes() {
        return playtimes;
    }

    public void setPlaytimes(int playtimes) {
        this.playtimes = playtimes;
    }

    /**
     * App 凭证，具体计算方式见下注
     */
    private String sig;

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    /**
     * 请求发起时间，unix 时间戳（单位：秒），如果和系统时间相差超过 10 分钟则会返回失败
     */
    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    /**
     * 用户的 session 内容，腾讯 server 回包中会原样返回，可选字段，不需要就填空
     */
    private String ext;

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public SendvoiceReq() {
    }

    public SendvoiceReq(Tel tel, String msg, String sig, long time) {
        this.tel = tel;
        this.msg = msg;
        this.playtimes = 3;
        this.sig = sig;
        this.time = time;
        this.ext = "";
    }
}
