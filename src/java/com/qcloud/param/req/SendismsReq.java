package com.qcloud.param.req;

import org.nutz.json.JsonField;

import java.util.List;

/**
 * Created on 2018/1/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class SendismsReq {

    /**
     * 国际电话号码，格式依据 e.164
     * 标准为: +[国家码][手机号] ，示例如：+8613711112222， 其中前面有一个 + 符号 ，86 为国家码，13711112222 为手机号
     */
    private String tel;

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 短信签名，如果使用默认签名，该字段可缺省
     */
    private String sign;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    /**
     * 模板 ID，在控制台审核通过的模板 ID
     */
    @JsonField("tpl_id")
    private int tplId;

    public int getTplId() {
        return tplId;
    }

    public void setTplId(int tplId) {
        this.tplId = tplId;
    }

    /**
     * 模板参数，若模板没有参数，请提供为空数组
     */
    private List<String> params;

    public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }

    /**
     * App 凭证，具体计算方式见下注
     */
    private String sig;

    public String getSig() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig = sig;
    }

    /**
     * 请求发起时间，unix 时间戳（单位：秒），如果和系统时间相差超过 10 分钟则会返回失败
     */
    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    /**
     * 用户的 session 内容，腾讯 server 回包中会原样返回，可选字段，不需要就填空
     */
    private String extend;

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    /**
     * 短信码号扩展号，格式为纯数字串，其他格式无效。默认没有开通，开通请联系 腾讯云短信技术支持
     */
    private String ext;

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public SendismsReq() {
    }

    public SendismsReq(String tel, String sign, int tplId, List<String> params, String sig, long time) {
        this.tel = tel;
        this.sign = sign;
        this.tplId = tplId;
        this.params = params;
        this.sig = sig;
        this.time = time;
        this.extend = "";
        this.ext = "";
    }
}
