package com.qcloud.param.biz;

/**
 * Created on 2018/1/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class Tel {

    /**
     * 国家码
     */
    private String nationcode;

    public String getNationcode() {
        return nationcode;
    }

    public void setNationcode(String nationcode) {
        this.nationcode = nationcode;
    }

    /**
     * 手机号码
     */
    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Tel() {
    }

    public Tel(String mobile) {
        this.nationcode = "86";
        this.mobile = mobile;
    }

    public Tel(String nationcode, String mobile) {
        this.nationcode = nationcode;
        this.mobile = mobile;
    }
}
