package com.qcloud.util;

import com.jigoucai.util.HttpUtil;
import com.qcloud.param.req.SendismsReq;
import com.qcloud.param.req.SendsmsReq;
import com.qcloud.param.req.SendvoiceReq;
import com.qcloud.param.resp.SendismsResp;
import com.qcloud.param.resp.SendsmsResp;
import com.qcloud.param.resp.SendvoiceResp;
import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.nutz.lang.Times;
import org.nutz.lang.random.R;
import org.nutz.log.Log;
import org.nutz.log.Logs;

/**
 * 腾讯云短信发送工具类
 * Created on 2018/1/19
 *
 * @author Jianghao(howechiang @ gmail.com)
 */
public class SendUtil {

    protected static final Log log = Logs.get();

    /**
     * 指定模板单发短信
     * 给用户发短信验证码、短信通知，营销短信（内容长度不超过450字）。
     *
     * @param sdkappid
     * @param appkey
     * @param req
     * @return
     */
    public static SendsmsResp sendsms(String sdkappid, String appkey, SendsmsReq req) {

        try {
            if (Strings.isBlank(sdkappid)) {
                return null;
            }
            if (Lang.isEmpty(req)) {
                return null;
            }
            String random = R.captchaNumber(10);
            req.setTime(Times.getTS());
            req.setSig(Lang.sha256("appkey=" + appkey + "&random=" + random + "&time=" + req.getTime() + "&mobile=" + req.getTel().getMobile()));
            String json = HttpUtil.post("https://yun.tim.qq.com/v5/tlssmssvr/sendsms?sdkappid=" + sdkappid + "&random=" + random, Json.toJson(req, JsonFormat.compact()));
            if (json.indexOf("result") >= 0) {
                SendsmsResp resp = Json.fromJson(SendsmsResp.class, json);
                return resp;
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 指定模板单发短信ISMS
     * 用户发送国际短信专用接口。
     *
     * @param sdkappid
     * @param appkey
     * @param req
     * @return
     */
    public static SendismsResp sendisms(String sdkappid, String appkey, SendismsReq req) {

        try {
            if (Strings.isBlank(sdkappid)) {
                return null;
            }
            if (Lang.isEmpty(req)) {
                return null;
            }
            String random = R.captchaNumber(10);
            req.setTime(Times.getTS());
            req.setSig(Lang.sha256("appkey=" + appkey + "&random=" + random + "&time=" + req.getTime() + "&tel=" + req.getTel()));
            String json = HttpUtil.post("https://yun.tim.qq.com/v5/tlssmssvr/sendsms?sdkappid=" + sdkappid + "&random=" + random, Json.toJson(req, JsonFormat.compact()));
            if (json.indexOf("result") >= 0) {
                SendismsResp resp = Json.fromJson(SendismsResp.class, json);
                return resp;
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * 发送语音验证码
     * 给国内用户发语音验证码（支持英文字母、数字及组合）。
     *
     * @param sdkappid
     * @param appkey
     * @param req
     * @return
     */
    public static SendvoiceResp sendvoice(String sdkappid, String appkey, SendvoiceReq req) {

        try {
            if (Strings.isBlank(sdkappid)) {
                return null;
            }
            if (Lang.isEmpty(req)) {
                return null;
            }
            String random = R.captchaNumber(10);
            req.setTime(Times.getTS());
            req.setSig(Lang.sha256("appkey=" + appkey + "&random=" + random + "&time=" + req.getTime() + "&mobile=" + req.getTel().getMobile()));
            String json = HttpUtil.post("https://yun.tim.qq.com/v5/tlsvoicesvr/sendvoice?sdkappid=" + sdkappid + "&random=" + random, Json.toJson(req, JsonFormat.compact()));
            if (json.indexOf("result") >= 0) {
                SendvoiceResp resp = Json.fromJson(SendvoiceResp.class, json);
                return resp;
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }
}
