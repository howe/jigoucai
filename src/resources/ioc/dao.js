var ioc = {
    dataSource2: {
        factory: "$conf#make",
        args: ["com.alibaba.druid.pool.DruidDataSource", "jdbc2."],
        type: "com.alibaba.druid.pool.DruidDataSource",
        events: {
            create: "init",
            depose: "close"
        }
    },
    dao2: {
        type: "org.nutz.dao.impl.NutDao",
        args: [{refer: "dataSource2"}]
    }
};